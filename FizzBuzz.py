def fizz_buzz(limit, replace_option=((3, 'Fizz'), (5, 'Buzz'))):
    """
    Функция выводит результат игры FizzBuzz для заданого максимального 
    лимита и указанных вариантов замен.
    Args:
        limit(int): верхняя граница, до которой надо вывести результат
        replace_option(tuple((int, str), (int, str))): варианты замен 
            элементов в игре
    Returns:
        None
    """
    for number in range(1, limit+1):
        result = ""

        for option in replace_option:
            if number % option[0] == 0:
                result += option[1]
        print(result or number)
